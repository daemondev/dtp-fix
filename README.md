## INSTALL

useful for Deepin or Debian based distros, touch pad fix.
clone this repository and follow next steps.

* Inner in this directory, only run as sudo:

```bash
sudo ./installer
```

* or

```bash
sudo dpkg -i *.deb
```
* or in a single command:
 
```bash
sudo git clone https://gitlab.com/daemondev/dtp-fix && cd dtp-fix && sudo dpkg -i *.deb
```

by daemonDEV
